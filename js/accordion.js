$(document).ready(function(){
	var parentlist = $('.parent_list')
	var list = $('.list_item');
	var gorod = $('.gorod');
	var popup = $('.popup');
	parentlist.hide().prev().click(function(){
		$(parentlist).not(this).slideUp();
		$(this).next().not(":visible").slideDown();
	});
	gorod.click(function(){
		$(gorod).removeClass('active');
		$(this).toggleClass('active');
	});
});